import logo from './logo.svg';
import './App.css';
import {
  Stack, TextField, IconButton, Button, TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { Box } from '@mui/system';
import React from 'react';
import Axios from "axios"
import Paper from '@mui/material/Paper';

function App() {
  const [numbers, setNumbers] = React.useState({
    data: [],
    uploading: false,
    tempNumber: 0,
    result: {
      sum: null,
      avg: null,
      stdev: null,
    }
  });

  const handleNumberAppend = () => {
    setNumbers(state => ({
      ...state,
      tempNumber: 0,
      data: [...state.data, state.tempNumber],
    }));
  }

  const handleNumberRemove = () => {
    setNumbers(state => ({
      ...state,
      data: state.data.slice(0, -1),
    }));
  }

  const handleTempNumberChange = (e) => {
    const { value } = e.target

    setNumbers(state => ({
      ...state,
      tempNumber: value,
    }));
  }

  const handleNumberSubmit = () => {
    if (numbers.data.length <= 0) {
      alert("Provide with atleast one number.");
      return;
    }
    setNumbers(state => ({
      ...state,
      uploading: true,
    }));


    Axios.post("http://localhost:4000/do-maths", {
      numbers: numbers.data.toString(),
    }).then(res => {
      setNumbers(state => ({
        ...state,
        result: res.data,
        uploading: false,
      }));
    }).catch(err => {
      setNumbers(state => ({
        ...state,
        uploading: false,
      }));
      alert("Something went wrong. Please try again.");
    })
  }

  return (
    <Stack sx={{ minHeight: "100vh", alignItems: "center", justifyContent: "center" }}>

      <Stack direction="row" spacing={2}>
        <TextField value={numbers.tempNumber} placeholder='Enter Number' size="small" type="number" onChange={handleTempNumberChange} />
        <IconButton onClick={handleNumberAppend}>
          <AddIcon />
        </IconButton>
      </Stack>

      {/* Show numbers */}
      <Stack direction="row" spacing={2} alignItems="center">
        <h5>
          Number To be uploaded
        </h5>
        <p>
          ( {numbers.data.length > 0 ? numbers.data.toString() : "No numbers"} )
        </p>
        <Button size="small" variant="contained" onClick={handleNumberRemove}>
          Remove last number
        </Button>
      </Stack>

      {/* Get results */}
      <Button size="small" variant="contained" color="success" onClick={handleNumberSubmit} disabled={numbers.uploading}>
        Get Arithmetic Result
      </Button>

      <TableContainer component={Paper} sx={{ width: "auto" }}>
        <Table sx={{ maxWidth: 650 }}>
          <TableHead>
            <TableRow>
              <TableCell>
                Sum
              </TableCell>
              <TableCell>
                Average
              </TableCell>
              <TableCell>
                Standard Deviation
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                {numbers.result.sum ? numbers.result.sum : "No result"}
              </TableCell>
              <TableCell>
                {numbers.result.avg ? numbers.result.avg : "No result"}
              </TableCell>
              <TableCell>
                {numbers.result.stdev ? numbers.result.stdev : "No result"}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Stack>
  );
}

export default App;
